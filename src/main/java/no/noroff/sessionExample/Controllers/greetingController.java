package no.noroff.sessionExample.Controllers;

import no.noroff.sessionExample.Util.Password;
import no.noroff.sessionExample.Util.SessionKeeper;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class greetingController {

    @GetMapping(value = "/login")
    public String login(@CookieValue(value = "foreverCookie", defaultValue = "") String foreverCookie, HttpServletRequest request, HttpSession session, Model model) {
        model.addAttribute("sessionId", session.getId());
        model.addAttribute("loginStatus", SessionKeeper.getInstance().CheckSession(session.getId()));
        model.addAttribute("Password", new Password());
        model.addAttribute("cookieFound", !foreverCookie.equals(""));
        return "login";
    }

    //This doesn't check for the "foreverCookie", so the model will contain a null value
    @GetMapping(value = "/special")
    public String special(
            HttpServletRequest request,
            HttpServletResponse response,
            HttpSession session,
            Model model) {

        model.addAttribute("sessionId", session.getId());

        model.addAttribute("loginStatus", SessionKeeper.getInstance().CheckSession(session.getId()));

        if(SessionKeeper.getInstance().CheckSession(session.getId()))
        {
            //import javax.servlet.http.Cookie; (there are different cookie options available
            Cookie secretCookie = new Cookie("secret", "Here_is_a_secret_cookie");
            secretCookie.setMaxAge(120); // in seconds
            response.addCookie(secretCookie);
        }

        return "special";
    }

    @PostMapping(value = "/saveSession")
    public String add(
            @RequestParam(value="action", required=true) String action,
            @ModelAttribute("Password") Password password,
            HttpServletRequest request,
            HttpSession session,
            Model model) {
    if(action.equals("login")){
        if(password.value.equals("cat")){
            System.out.println("password correct");
            SessionKeeper.getInstance().AddSession(session.getId());
        }
        else
        {
            System.out.println("login failed");
        }
    }
    else if(action.equals("logout")) {
        SessionKeeper.getInstance().RemoveSession(session.getId());
        System.out.println("logged out");
    }
    return "redirect:/login";
    }

    @GetMapping(value = "/secret")
    public String secret(@CookieValue(value = "secret", defaultValue = "") String secret,
            HttpSession session) {

        // User needs the secret cookie and must be logged in
        if(secret.equals("Here_is_a_secret_cookie") && SessionKeeper.getInstance().CheckSession(session.getId())){
            return "secret";
        }
        else
        {
            return "redirect:/login";
        }
    }
}

